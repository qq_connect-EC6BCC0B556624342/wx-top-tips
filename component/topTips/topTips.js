// component/topTips/topTips.js
// js代码
Component({
  properties: {
    errorMsg: {
      type: String,
      value: ''
    },
    successMsg: {
      type: String,
      value: ''
    },
    infoMsg: {
      type: String,
      value: ''
    },
    delay: {
      type: Number,
      value: 2000
    },
    navBarHeight: {
      type: String,
      value: ""
    }
  },

  data: {
    show: false,
    msg: '',
    typeName: 'error'
  },

  methods: {
    setMsg(type, value) {
      if (value)
        this.setData({
          show: true,
          msg: value,
          typeName: type
        })
    }
  },
  observers: {
    errorMsg: function (value) {
      this.setMsg('error',value)
    },
    successMsg: function (value) {
      this.setMsg('success',value)
    },
    infoMsg: function (value) {
      this.setMsg('info',value)
    },
  }
})