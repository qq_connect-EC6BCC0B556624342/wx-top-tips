const app = getApp()

Page({
  data: {
    errMsg: "",
    successMsg: "",
    infoMsg: "",
    value: "",
  },
  showTopTips(e) {
    this.setData({
      [e.currentTarget.dataset.name]: this.data.value
    })
  },
})