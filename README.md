# wx-topTips
微信weui Toptips组件优化版
介绍地址：[微信小程序WeUI Toptips使用和封装改进](http://t.csdn.cn/T3YGD)

## 属性列表和说明
|  属性名  | 说明  | 类型  |
|  ----  | ----  | ----  |
| errorMsg | error类型提示内容，赋值显示 | String |
| successMsg | success类型提示内容，赋值显示 | String |
| infoMsg | info类型提示内容，赋值显示 | String |
| navBarHeight | Toptips顶部的距离 | String |
| delay | 提示内容显示后隐藏的ms值 | Number |